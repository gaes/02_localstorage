# LOCAL STORAGE #
We can use storage API available for storing data on the client apps. This will help the usage of the app when the user is offline and it can also improve performance. Since this tutorial is for beginners, we will show you how to use local storage. In one of our later tutorials, we will show you the other plugins that can be used.

https://www.tutorialspoint.com/cordova/cordova_storage.htm